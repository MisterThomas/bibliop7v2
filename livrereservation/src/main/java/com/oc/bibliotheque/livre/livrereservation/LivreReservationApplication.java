package com.oc.bibliotheque.livre.livrereservation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableDiscoveryClient
@EnableScheduling
@EnableFeignClients("com.oc.bibliotheque.livre.livrereservation")
@EnableConfigurationProperties
public class LivreReservationApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(LivreReservationApplication.class, args);
	}

}
