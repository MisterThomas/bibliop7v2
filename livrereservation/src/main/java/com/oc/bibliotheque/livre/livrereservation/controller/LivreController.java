package com.oc.bibliotheque.livre.livrereservation.controller;

import com.oc.bibliotheque.livre.livrereservation.model.Emprunt;
import com.oc.bibliotheque.livre.livrereservation.model.Livre;
import com.oc.bibliotheque.livre.livrereservation.repository.EmpruntRepository;
import com.oc.bibliotheque.livre.livrereservation.repository.LivreRepository;
import com.oc.bibliotheque.livre.livrereservation.service.LivreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class LivreController {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LivreService livreService;

    @Autowired
    LivreRepository livreRepository;

    @Autowired
    EmpruntRepository empruntRepository;

    @GetMapping("/api/Livre/listeLivres")
    public List<Livre> listeLivres(){
        log.info("liste livres");
        List<Livre> livres = livreRepository.findAll();
        return livres;
    }



    @GetMapping("/api/Livre/test")
    public List<Livre> livres(String username){
        log.info("livre username");
        List<Livre> livres = livreService.getLivrebyLivre(username);
        return livres;
    }



    @PostMapping("/api/Livre/listeLivres/recherche")
    @ResponseBody
    public List<Livre> listeLivresAuteur(@RequestParam(value = "auteur", required = false) String auteur, @RequestParam(value = "titre", required = false) String titre){
        log.info("liste livres auteur by titre");
        List<Livre> livres = livreRepository.findByAuteurOrTitre(auteur, titre);
        return livres;
    }




    @GetMapping(value="/api/Livre/{id}")
    public Optional<Livre> detailLivre(@PathVariable long id){
        log.info("livre id");
        return livreService.getLivreById(id);
    }



}
