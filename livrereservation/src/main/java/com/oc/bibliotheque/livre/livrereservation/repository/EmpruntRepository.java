package com.oc.bibliotheque.livre.livrereservation.repository;



import com.oc.bibliotheque.livre.livrereservation.model.Emprunt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface EmpruntRepository extends JpaRepository<Emprunt, Long> {


    List<Emprunt> findByUsername(String emprunt);
    List<Emprunt> findByLivre(Emprunt emprunt);


    @Query("SELECT e FROM Emprunt  e WHERE e.date_fin_prevu = e.date_fin_prevu")
    List<Emprunt> findALLByAndDate_fin_prevu(String date);



}
