package com.oc.bibliotheque.livre.livrereservation.batch;

import com.oc.bibliotheque.livre.livrereservation.beans.MembreBean;
import com.oc.bibliotheque.livre.livrereservation.model.Emprunt;
import com.oc.bibliotheque.livre.livrereservation.proxies.MembreProxy;
import com.oc.bibliotheque.livre.livrereservation.repository.EmpruntRepository;
import com.oc.bibliotheque.livre.livrereservation.service.EmpruntService;
import com.oc.bibliotheque.livre.livrereservation.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
public class Batch {

    @Autowired
    MailService notificationService;


    @Autowired
    MembreProxy membreProxy;

    @Autowired
    EmpruntRepository empruntRepository;

    @Autowired
    EmpruntService empruntService;


 //  @Scheduled(cron = "*/10 * * * * *")
   @Scheduled(cron = "0 0/30 19 * * *")
  public void batchNotification() {
       Date date = Calendar.getInstance().getTime();
       DateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
       String date_fin_prevu = dateFormat.format(date);

       Calendar calendar = Calendar.getInstance();

       List<Emprunt> emprunts = empruntRepository.findALLByAndDate_fin_prevu(date_fin_prevu);

       Iterator<Emprunt> iterEmprunt = emprunts.iterator();

       while (iterEmprunt.hasNext()) {

           Emprunt empruntBean = iterEmprunt.next();

           Date date1 = empruntBean.getDate_emprunt();
           Calendar c = Calendar.getInstance();
           c.setTime(date1);
           c.add(Calendar.WEEK_OF_MONTH, 3);
           date1 = c.getTime();


           if (calendar.getTime().compareTo(date1) >= 0 && (empruntBean.isEmprunt_retour() == false)) {
               MembreBean membreBean = membreProxy.usernameMembre(empruntBean.getUsername());
               membreBean.getEmail();
               //Receiver's email address
               try {
                   notificationService.sendEmail(membreBean);
               } catch (MailException mailException) {
                   System.out.println(mailException);
               }
               System.out.println("un mail de relance vient d'etre envoye a " + empruntBean.getUsername());
           } else if (empruntBean.isEmprunt_retour() == true) {
               System.out.println("livre rendu");
           }
       }
       System.out.println("livre encours d'emprunt");
   }
  }
