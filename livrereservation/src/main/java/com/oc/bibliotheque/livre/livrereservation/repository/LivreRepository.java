package com.oc.bibliotheque.livre.livrereservation.repository;

import com.oc.bibliotheque.livre.livrereservation.model.Livre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface LivreRepository  extends JpaRepository<Livre, Long> {

    List<Livre> findByUsername(String livre);

    List<Livre> findByTitre(String titre);


    List<Livre> findByAuteur(String auteur);

    List<Livre> findByAuteurOrTitre(String auteur, String titre);


}
