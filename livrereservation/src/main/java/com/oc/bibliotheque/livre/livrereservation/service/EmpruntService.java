package com.oc.bibliotheque.livre.livrereservation.service;

import com.oc.bibliotheque.livre.livrereservation.model.Emprunt;
import com.oc.bibliotheque.livre.livrereservation.repository.EmpruntRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EmpruntService  implements  IEmpruntService {


    @Autowired
    EmpruntRepository empruntRepository;

@Override
     public List<Emprunt> getEmpruntByUsername(String emprunt){
        return     empruntRepository.findByUsername(emprunt);
    }



    @Override
    public Optional<Emprunt> getEmpruntById(long id) {
        return  empruntRepository.findById(id);
    }




    @Override
    public void addEmprunt(String username, boolean bonus_semaine,boolean emprunt_retour, Date date_emprunt, String date_fin_prevu, String date_semaine_bonus, boolean isDone) {
        empruntRepository.save(new Emprunt( username, bonus_semaine, emprunt_retour, date_emprunt, date_fin_prevu, date_semaine_bonus, isDone));
    }


    @Override
    public Emprunt updateEmprunt(Emprunt emprunt) {
        emprunt=  empruntRepository.save(emprunt);

            return emprunt;
    }

    @Override
    public Emprunt retourEmprunt(Emprunt emprunt) {
         emprunt = empruntRepository.save(emprunt);
         return  emprunt;
    }

    @Override
    public Emprunt saveEmprunt(Emprunt emprunt) {
       emprunt = empruntRepository.save(emprunt);
        return emprunt;
    }
}
