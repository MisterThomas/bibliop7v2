package com.clientui.services;

import com.clientui.beans.MembreBean;
import com.clientui.proxies.MembreProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;


@Service("MembreService")
public class MembreClientUIService implements UserDetailsService {

    @Autowired
    MembreProxy membreProxy;


    @Autowired
     static BCryptPasswordEncoder passwordEncoder;



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        MembreBean  membreBean =  membreProxy.usernameMembre(username);
        if (membreBean == null) {
            throw new UsernameNotFoundException("No user present with username : " + membreBean);
        } else {
            return (UserDetails) membreBean;
        }
    }

}

