package com.clientui.beans;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.context.annotation.Bean;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
@Entity
public class LivreBean extends EmpruntBean {

@Id
    private long id;

    private String username;

    private String auteur;

    private String titre;

    private int  nombre_disponible;

    private boolean reservation_disponible;




    public LivreBean(String user, String auteur,String titre,  int nombre_disponible,  boolean reservation_disponible,  boolean isDone) {
        super();
        // this.id = id;
        //@JsonProperty("id") final long id,
        this.username = user;
        this.auteur =auteur;
        this.titre = titre;
        this.nombre_disponible = nombre_disponible;
        this.reservation_disponible = reservation_disponible;
    }



    @OneToMany(mappedBy = "livre", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Set<EmpruntBean> empruntBean = new HashSet<>();

    public Set<EmpruntBean> getEmpruntBean() {
        return empruntBean;
    }

    public void setEmpruntBean(Set<EmpruntBean> empruntBean) {
        this.empruntBean = empruntBean;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getNombre_disponible() {
        return nombre_disponible;
    }

    public void setNombre_disponible(int nombre_disponible) {
        this.nombre_disponible = nombre_disponible;
    }


    public boolean isReservation_disponible() {
        return reservation_disponible;
    }

    public void setReservation_disponible(boolean reservation_disponible) {
        this.reservation_disponible = reservation_disponible;
    }


    @Override
    public String toString() {
        return "LivreBean{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", auteur='" + auteur + '\'' +
                ", titre='" + titre + '\'' +
                ", nombre_disponible=" + nombre_disponible +
                ", reservation_disponible=" + reservation_disponible +
                ", empruntBean=" + empruntBean +
                ", date_emprunt=" + date_emprunt +
                ", date_fin_prevu='" + date_fin_prevu + '\'' +
                ", date_semaine_bonus='" + date_semaine_bonus + '\'' +
                '}';
    }
}
