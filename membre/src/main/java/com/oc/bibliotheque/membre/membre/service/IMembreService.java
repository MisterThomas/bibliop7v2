package com.oc.bibliotheque.membre.membre.service;

import com.oc.bibliotheque.membre.membre.model.Membre;

import java.util.List;
import java.util.Optional;

public interface IMembreService {
    void save();

    Membre findByUsername(String username);

    List<Membre> getMembreByUsername(String username);


}
