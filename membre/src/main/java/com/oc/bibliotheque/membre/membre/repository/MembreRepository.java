package com.oc.bibliotheque.membre.membre.repository;

import com.oc.bibliotheque.membre.membre.model.Membre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface MembreRepository extends JpaRepository<Membre, Long> {

    List<Membre> findByUsername(String username);
}
